<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
</head>
<body>
    <div class="header">
        <div class="container">
            <div class="header__inner">
                <div class="logo">
                    <a href="/">LOGO</a>
                </div>
                <div class="header__inner-right">
                    <nav class="nav">
                        <ul>
                            <li class="nav__item">
                                <a href="/">Home</a>
                            </li>
                            <li class="nav__item">
                                <a href="/services">Services</a>
                            </li>
                            <li class="nav__item">
                                <a href="/about">About</a>
                            </li>
                            <li class="nav__item">
                                <a href="/contact">Contact</a>
                            </li>
                            <li class="nav__item">
                                <a href="/faq">FAQ</a>
                            </li>
                        </ul>
                    </nav>
                    <a class="base__button base__button-nav" href="/signup">SIGN UP</a>
                    <div class="toggle__menu">
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('content')

    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="{{ asset('/js/main.js') }}"></script>
</body>
</html>