@extends('layouts.layout')

@section('content')
<div class="container">
  <div class="signup">
    <div class="alert">
      @if(session()->has('message'))
      <div class="alert-success">
          {{ session()->get('message') }}
      </div>
    @endif
    </div>
    <form class="signup__form" method="POST" action="/signup">
      @csrf
      <div class="form__block">
        <label class="form__label" for="name">Name</label>
        <div class="input__wrapper">
          <input class="form__input" id="name" name="name" type="text" value="{{ old('name') }}" />
          @error('name')
          <span class="invalid__feedback">
            {{ $message }}
          </span>
          @enderror
        </div>
      </div>
      <div class="form__block">
        <label class="form__label" for="email">E-mail</label>
        <div class="input__wrapper">
          <input class="form__input" id="email" name="email" type="text" value="{{ old('email') }}" />
          @error('email')
          <span class="invalid__feedback">
            {{ $message }}
          </span>
          @enderror
        </div>
      </div>
      <button class="base__button base__button-signup" type="submit">Submit</button>
    </form> 
  </div>
</div>
@endsection
