@extends('layouts.layout')

@section('content')
<section class="intro">
    <div class="container">
        <div class="intro__inner">
            <div class="intro__inner-info">
                <h1 class="intro__title">
                    Digital
                </h1>
                <h3 class="intro__suptitle">
                    Marketing
                </h3>
                <div class="intro__text">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni atque quod neque sed corporis laboriosam, recusandae ab cum harum repudiandae ratione, autem odio nobis aspernatur eum mollitia similique quo iste!
                </div>
                <a class="base__button base__button--intro" href="/learn-more">Learn more</a>
            </div>
            <div class="intro__inner-img">
                <img src="{{ asset('/img/illustration.jpg') }}" alt="">
            </div>
        </div>
    </div>
</section>
@endsection
